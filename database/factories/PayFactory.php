<?php

namespace Database\Factories;

use App\Models\Pay;
use Illuminate\Database\Eloquent\Factories\Factory;

class PayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pay::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'libelle' => $this->faker->country(),
            'description' => $this->faker->sentence(),
            'code_indicatif' => $this->faker->title(),
            'continent' => $this->faker->title(),
            'population' => $this->faker->randomElement([100000, 200000, 3500000, 19990000]),
            'capitale' => $this->faker->city(),
            'monnaie' => $this->faker->randomElement(["XOF", "EUR", "DOLLAR"]),
            'langue' => $this->faker->randomElement(["FR", "EN", "AR", "ES"]),
            'superficie' => $this->faker->randomElement([300000, 500000, 1500000, 19999000]),
            'est_laique' => $this->faker->boolean(),

             // password

            //
        ];
    }
}
