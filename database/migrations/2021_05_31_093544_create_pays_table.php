<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pays', function (Blueprint $table) {
            $table->id();
            $table->string('libelle');
            $table->string('description')->require;
            $table->string('code_indicatif');
            $table->string('continent');
            $table->integer('population');
            $table->string('capitale')->require;
            $table->string('monnaie')->require;
            $table->string('langue');
            $table->integer('superficie');
            $table->boolean('est_laique');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pays');
    }
}
