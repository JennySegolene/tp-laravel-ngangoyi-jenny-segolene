@extends('layouts.main')
@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Pays</h4>
            <p class="card-category"> Liste des pays</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <tr><th>
                    ID
                  </th>
                  <th>
                    Libelle
                  </th>
                  <th>
                    Description
                  </th>
                  <th>
                    code_indicatif
                  </th>
                  <th>
                    continent
                  </th>
                  <th>
                    population
                  </th>
                  <th>
                    capitale
                  </th>
                  <th>
                    monnaie
                  </th>
                  <th>
                    langue
                  </th>
                  <th>
                    superficie
                  </th>
                  <th>
                    est_laique
                  </th>

                </tr></thead>
                <tbody>

                    @foreach ($pays as $pays)
                    <tr>
                     <td>{{$pays->id}}</td>
                     <td>{{$pays->libelle}}</td>
                     <td>{{$pays->description}}</td>
                     <td>{{$pays->code_indicatif}}</td>
                     <td>{{$pays->continent}}</td>
                     <td>{{$pays->population}}</td>
                     <td>{{$pays->capitale}}</td>
                     <td>{{$pays->monnaie}}</td>
                     <td>{{$pays->langue}}</td>
                     <td>{{$pays->superficie}}</td>
                     <td>{{$pays->est_laique}}</td>


                   </tr>


                    @endforeach

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

@endsection
