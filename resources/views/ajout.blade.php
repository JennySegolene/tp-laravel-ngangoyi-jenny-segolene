@extends('layouts.main')
@section()

<div class="col-md-8">
    <div class="card">
      <div class="card-header card-header-primary">
        <h4 class="card-title">Ajouet un pays</h4>
        <p class="card-category">Ajout</p>
      </div>
      <div class="card-body">
        <form>
          <div class="row">
            <div class="col-md-5">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Libelle</label>
                <input type="text" class="form-control" disabled="">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Description</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">code_indicatif</label>
                <input type="email" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">continent</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">population</label>
                <input type="text" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">capitale</label>
                <input type="text" class="form-control">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">monnaie</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">langue</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">superficie</label>
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">est_laique</label>
                  <input type="text" class="form-control">
                </div>
              </div>
          </div>

          <button type="submit" class="btn btn-primary pull-right">Enregistrer</button>
          <div class="clearfix"></div>
        </form>
      </div>
    </div>
  </div>



@endsection
